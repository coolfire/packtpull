**UPDATE**: This script no longer works since packtpub decided to implement recaptcha validation on their free ebooks.

# Packtpull

This nigthmarejs script can be used to automatically log in to your packtpub.com account every day and click the link to get the free ebook of the day. No more logging in by hand every day and being sad when you forgot a day.


## Setup & Run
You need nodejs installed and the nightmare library which can be installed through npm: `npm install nightmare`.
If you want to run this script through a cronjob on a headless server you will need xvfb and call it with the xvfb-run wrapper.

## Functionality
At this point the script only clicks the link to claim your free ebook. Functionality to download the ebook automatically may be added later. If you've built the script out to include this already, please send me a merge request/patch/updated script.

# Note
I'm not sure how happy packtpub will be about all this. Maybe they don't mind but don't hold me responsible if one day they get mad and close your account.
