var email    = 'test@example.com';
var password = 'foobar';

var Nightmare = require('nightmare');
//var nightmare = Nightmare({ show: true });
var nightmare = Nightmare();

nightmare
  .goto('https://www.packtpub.com/packt/offers/free-learning#')
  .wait('.login-popup')
  .click('.login-popup')
  .wait('input#email')
  .type('input#email', email)
  .type('input#password', password)
  .click('#edit-submit-1')
  .wait('a[href*="/freelearning-claim/"] input.form-submit')
  .wait(2000)
  .click('a[href*="/freelearning-claim/"]')
  .wait('#product-account-list')
  .wait(2000)

  .then(nightmare.end())
  .catch((e) => console.dir(e));
